# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: 2016-03-19 13:25+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Russian (http://www.transifex.com/ubuntu-mate/ubuntu-mate-welcome/language/ru/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ru\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: community.html:17
msgid "Community"
msgstr ""

#: community.html:25
msgid "Forum"
msgstr ""

#: community.html:26
msgid ""
"To play an active role and help shape the direction of Ubuntu MATE, the "
"forum is the best place to go. From here, you can talk with the Ubuntu MATE "
"team and other members of our growing community."
msgstr ""

#: community.html:30
msgid ""
"It appears you are not connected to the Internet. Please check your "
"connection to access this content."
msgstr ""

#: community.html:31
msgid "Sorry, Welcome was unable to establish a connection."
msgstr ""

#: community.html:32
msgid "Retry"
msgstr ""

#: community.html:37
msgid "Social Networks"
msgstr ""

#: community.html:38
msgid "Ubuntu MATE is active on the following social networks."
msgstr ""
