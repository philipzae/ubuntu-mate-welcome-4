# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# bf5man <bf5man@gmail.com>, 2016
# Clement Krys <krys.clement@gmail.com>, 2016
# Matthieu S <utybo@users.noreply.github.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-06-14 18:20+0100\n"
"PO-Revision-Date: 2016-06-11 04:29+0000\n"
"Last-Translator: Clement Krys <krys.clement@gmail.com>\n"
"Language-Team: French (France) (http://www.transifex.com/ubuntu-mate/ubuntu-"
"mate-welcome/language/fr_FR/)\n"
"Language: fr_FR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: ubuntu-mate-welcome:71
msgid ""
"Software changes are in progress. Please allow them to complete before "
"closing Welcome."
msgstr ""
"Des modifications logicielles sont en cours. Veuillez laisser Bienvenue "
"terminer ces opérations avant de fermer la fenêtre."

#: ubuntu-mate-welcome:72 ubuntu-mate-welcome:2489
msgid "OK"
msgstr "OK"

#: ubuntu-mate-welcome:159 ubuntu-mate-welcome:169
msgid "Successfully performed fix."
msgstr "Correction appliquée avec succès"

#: ubuntu-mate-welcome:159
msgid "Any previously incomplete installations have been finished."
msgstr "Toutes les installations qui étaient incomplètes sont terminées."

#: ubuntu-mate-welcome:162 ubuntu-mate-welcome:172
msgid "Failed to perform fix."
msgstr "Echec de l'application de la correction."

#: ubuntu-mate-welcome:162
msgid "Errors occurred while finishing an incomplete installation."
msgstr ""
"Il y a eu des erreurs lors de la finalisation d'une installation incomplète"

#: ubuntu-mate-welcome:169
msgid "Packages with broken dependencies have been resolved."
msgstr "Les paquets avec des dépendances erronées ont été corrigés."

#: ubuntu-mate-welcome:172
msgid "Packages may still have broken dependencies."
msgstr "Les paquets peuvent encore avoir des dépendances erronées."

#: ubuntu-mate-welcome:181
msgid "Successfully updated cache."
msgstr "Cache correctement mis à jour."

#: ubuntu-mate-welcome:181
msgid "Software is now ready to install."
msgstr "Le logiciel est maintenant prêt à être installé."

#: ubuntu-mate-welcome:184
msgid "Failed to update cache."
msgstr "Echec de la mise à jour du cache."

#: ubuntu-mate-welcome:184
msgid "There may be a problem with your repository configuration."
msgstr "Il y a peut-être un problème avec la configuration du répertoire."

#: ubuntu-mate-welcome:240
msgid "Welcome will stay up-to-date."
msgstr "Bienvenue restera à jour."

#: ubuntu-mate-welcome:241
msgid ""
"Welcome and the Software Boutique are set to receive the latest updates."
msgstr ""
"Bienvenue et la Boutique de logiciels sont configurés pour recevoir les "
"dernières mises à jour."

#: ubuntu-mate-welcome:247
msgid "Everything is up-to-date"
msgstr "Tout est à jour"

#: ubuntu-mate-welcome:248
msgid "All packages have been upgraded to the latest versions."
msgstr "Tous les paquets ont été mis à jour à la dernière version."

#: ubuntu-mate-welcome:253
msgid "Installed"
msgstr "Installé"

#: ubuntu-mate-welcome:254
msgid "The application is now ready to use."
msgstr "L'application est maintenant prête à l'emploi."

#: ubuntu-mate-welcome:256
msgid "was not installed."
msgstr "n'a pas été installé."

#: ubuntu-mate-welcome:257 ubuntu-mate-welcome:267 ubuntu-mate-welcome:298
#: ubuntu-mate-welcome:308
msgid "The operation was cancelled."
msgstr "L'opération a été annulée."

#: ubuntu-mate-welcome:259
msgid "failed to install"
msgstr "n'a pas pu être installé"

#: ubuntu-mate-welcome:260
msgid "There was a problem installing this application."
msgstr "Il y a eu un problème pendant l'installation de cette application."

#: ubuntu-mate-welcome:263 ubuntu-mate-welcome:827
msgid "Removed"
msgstr "Désinstallé"

#: ubuntu-mate-welcome:264
msgid "The application has been uninstalled."
msgstr "Cette application a été désinstallée."

#: ubuntu-mate-welcome:266
msgid "was not removed."
msgstr "n'a pas été désinstallé."

#: ubuntu-mate-welcome:269
msgid "failed to remove"
msgstr "n'a pas pu être désinstallé."

#: ubuntu-mate-welcome:270
msgid "A problem is preventing this application from being removed."
msgstr "Un problème empêche cette application d'être supprimée."

#: ubuntu-mate-welcome:273
msgid "Upgraded"
msgstr "Mis à jour"

#: ubuntu-mate-welcome:274
msgid "This application is set to use the latest version."
msgstr "Cette application est réglée pour utiliser la dernière version."

#: ubuntu-mate-welcome:276
msgid "was not upgraded."
msgstr "n'a pas été mis à jour."

#: ubuntu-mate-welcome:277
msgid "The application will continue to use the stable version."
msgstr "Cette application continuera à utiliser la version stable."

#: ubuntu-mate-welcome:279
msgid "failed to upgrade"
msgstr "n'a pas pu être mis à jour."

#: ubuntu-mate-welcome:280
msgid "A problem is preventing this application from being upgraded."
msgstr "Un problème empêche cette application d'être mise à jour."

#: ubuntu-mate-welcome:294
msgid "applications have been installed"
msgstr "applications ont été installées."

#: ubuntu-mate-welcome:295
msgid "Your newly installed applications are ready to use."
msgstr "Votre nouvelle application est prête à l'emploi."

#: ubuntu-mate-welcome:297
msgid "Some applications were not installed."
msgstr "Des applications n'ont pas été installées."

#: ubuntu-mate-welcome:300
msgid "Some applications failed to install."
msgstr "Des applications n'ont pas pu être installées."

#: ubuntu-mate-welcome:301 ubuntu-mate-welcome:311
msgid "Software Boutique encountered a problem with one of the applications."
msgstr "La Boutique de logiciels a eu un problème avec l'une des applications."

#: ubuntu-mate-welcome:304
msgid "applications have been removed"
msgstr "applications ont été désinstallées."

#: ubuntu-mate-welcome:305
msgid "These applications are no longer present on your system."
msgstr "Ces applications ne sont plus présentes sur votre système."

#: ubuntu-mate-welcome:307
msgid "Some applications were not removed."
msgstr "Des applications n'ont pas été désinstallées."

#: ubuntu-mate-welcome:310
msgid "Some applications failed to remove."
msgstr "Des applications n'ont pas pu être installées"

#: ubuntu-mate-welcome:477
msgid "Blu-ray AACS database install succeeded"
msgstr "L'installation de la base de données Blu-ray AACS a réussi"

#: ubuntu-mate-welcome:478
msgid "Successfully installed the Blu-ray AACS database."
msgstr "La base de données Blu-ray AACS a été installée avec succès."

#: ubuntu-mate-welcome:478
msgid "Installation of the Blu-ray AACS database was successful."
msgstr "L'installation de la base de données Blu-ray AACS a été un succès."

#: ubuntu-mate-welcome:481
msgid "Blu-ray AACS database install failed"
msgstr "L'installation de la base de données Blu-ray AACS a échoué"

#: ubuntu-mate-welcome:482
msgid "Failed to install the Blu-ray AACS database."
msgstr "Échec de l'installation de la base de données Blu-ray AACS."

#: ubuntu-mate-welcome:482
msgid "Installation of the Blu-ray AACS database failed."
msgstr "L'installation de la base de données Blu-ray AACS a été un échec."

#: ubuntu-mate-welcome:767
msgid "Welcome"
msgstr "Bienvenue"

#: ubuntu-mate-welcome:768
msgid "Software Boutique"
msgstr "Boutique de Logiciels"

#: ubuntu-mate-welcome:769
msgid "Close"
msgstr "Fermer"

#: ubuntu-mate-welcome:770
msgid "Cancel"
msgstr "Annuler"

#: ubuntu-mate-welcome:773
msgid "Set to retrieve the latest software listings."
msgstr "Activer pour récupérer le catalogue de logiciels le plus récent."

#: ubuntu-mate-welcome:774
msgid "Retrieve the latest software listings."
msgstr "Récupérer le catalogue de logiciels le plus récent."

#: ubuntu-mate-welcome:775
msgid "Please wait while the application is being updated..."
msgstr "Veuillez attendre pendant que l'application est mise à jour..."

#: ubuntu-mate-welcome:776
msgid "Version:"
msgstr "Version :"

#: ubuntu-mate-welcome:779
msgid "This application is set to receive the latest updates."
msgstr "Cette application doit recevoir les dernières mises à jour."

#: ubuntu-mate-welcome:780
msgid "Alternative to:"
msgstr "Alternative à :"

#: ubuntu-mate-welcome:781
msgid "Hide"
msgstr "Masquer"

#: ubuntu-mate-welcome:782
msgid "Show"
msgstr "Afficher"

#: ubuntu-mate-welcome:783
msgid "Install"
msgstr "Installer"

#: ubuntu-mate-welcome:784
msgid "Reinstall"
msgstr "Réinstaller"

#: ubuntu-mate-welcome:785
msgid "Remove"
msgstr "Désinstaller"

#: ubuntu-mate-welcome:786
msgid "Upgrade"
msgstr "Mettre à Jour"

#: ubuntu-mate-welcome:787
msgid "Launch"
msgstr "Lancer"

#: ubuntu-mate-welcome:788
msgid "License"
msgstr "Licence"

#: ubuntu-mate-welcome:789
msgid "Platform"
msgstr "Plateforme"

#: ubuntu-mate-welcome:790
msgid "Category"
msgstr "Catégorie"

#: ubuntu-mate-welcome:791
msgid "Website"
msgstr "Site Web"

#: ubuntu-mate-welcome:792
msgid "Screenshot"
msgstr "Capture d'Écran"

#: ubuntu-mate-welcome:793
msgid "Source"
msgstr "Source"

#: ubuntu-mate-welcome:794
msgid "Canonical Partner Repository"
msgstr "Dépôt Partenaire Canonical"

#: ubuntu-mate-welcome:795
#, fuzzy
msgid "Ubuntu Multiverse Repository"
msgstr "Dépôt Ubuntu"

#: ubuntu-mate-welcome:796
msgid "Ubuntu Repository"
msgstr "Dépôt Ubuntu"

#: ubuntu-mate-welcome:797
msgid "Unknown"
msgstr "Inconnu"

#: ubuntu-mate-welcome:798
msgid "Undo Changes"
msgstr "Annuler les modifications"

#: ubuntu-mate-welcome:801
msgid "Installing..."
msgstr "Installation en cours..."

#: ubuntu-mate-welcome:802
msgid "Removing..."
msgstr "Désistallation en cours..."

#: ubuntu-mate-welcome:803
msgid "Upgrading..."
msgstr "Mise à jour en cours..."

#: ubuntu-mate-welcome:806
msgid "Accessories"
msgstr "Accessoires"

#: ubuntu-mate-welcome:807
msgid "Education"
msgstr "Éducation"

#: ubuntu-mate-welcome:808
msgid "Games"
msgstr "Jeux"

#: ubuntu-mate-welcome:809
msgid "Graphics"
msgstr "Graphismes"

#: ubuntu-mate-welcome:810
msgid "Internet"
msgstr "Internet"

#: ubuntu-mate-welcome:811
msgid "Office"
msgstr "Bureau"

#: ubuntu-mate-welcome:812
msgid "Programming"
msgstr "Programmation"

#: ubuntu-mate-welcome:813
msgid "Sound & Video"
msgstr "Son & Vidéo"

#: ubuntu-mate-welcome:814
msgid "System Tools"
msgstr "Outils systèmes"

#: ubuntu-mate-welcome:815
msgid "Universal Access"
msgstr "Accessibilité"

#: ubuntu-mate-welcome:816
msgid "Server One-Click Installation"
msgstr "Installation en un clic du serveur"

#: ubuntu-mate-welcome:817
msgid "Miscellaneous"
msgstr ""

#: ubuntu-mate-welcome:820
msgid "Search"
msgstr "Recherche"

#: ubuntu-mate-welcome:821
msgid "Please enter a keyword to begin."
msgstr "Merci d'entrer un mot-clé pour commencer."

#: ubuntu-mate-welcome:822
msgid "Please enter at least 3 characters."
msgstr "Merci d'entrer au moins 3 caractères."

#: ubuntu-mate-welcome:825
msgid "Added"
msgstr "Ajouté"

#: ubuntu-mate-welcome:826
msgid "Fixed"
msgstr "Corrigé"

#: ubuntu-mate-welcome:830
msgid "Queued for installation."
msgstr "En file d'attente pour installation."

#: ubuntu-mate-welcome:831
msgid "Queued for removal."
msgstr "En file d'attente pour désinstallation."

#: ubuntu-mate-welcome:832
msgid "Preparing to remove:"
msgstr "Préparation de la désinstallation : "

#: ubuntu-mate-welcome:833
msgid "Preparing to install:"
msgstr "Préparation de l'installation : "

#: ubuntu-mate-welcome:834
#, fuzzy
msgid "Removing:"
msgstr "Désistallation en cours..."

#: ubuntu-mate-welcome:835
#, fuzzy
msgid "Installing:"
msgstr "Installation en cours..."

#: ubuntu-mate-welcome:836
msgid "Updating cache..."
msgstr "Mise à jour du cache..."

#: ubuntu-mate-welcome:837
msgid "Verifying software changes..."
msgstr "Vérification de changements de logiciels..."

#: ubuntu-mate-welcome:838
msgid "Successfully Installed"
msgstr "Installation réussie"

#: ubuntu-mate-welcome:839
msgid "Failed to Install"
msgstr "Installation échouée"

#: ubuntu-mate-welcome:840
msgid "Successfully Removed"
msgstr "Désinstallation réussie"

#: ubuntu-mate-welcome:841
msgid "Failed to Remove"
msgstr "Désinstallation échouée"

#: ubuntu-mate-welcome:842
msgid "To be installed"
msgstr "A installer"

#: ubuntu-mate-welcome:843
msgid "To be removed"
msgstr "A désinstaller"

#: ubuntu-mate-welcome:1051
msgid "Servers"
msgstr "Serveurs"

#: ubuntu-mate-welcome:1085
msgid "Jan"
msgstr "Jan"

#: ubuntu-mate-welcome:1086
msgid "Feb"
msgstr "Fév"

#: ubuntu-mate-welcome:1087
msgid "Mar"
msgstr "Mar"

#: ubuntu-mate-welcome:1088
msgid "Apr"
msgstr "Avr"

#: ubuntu-mate-welcome:1089
msgid "May"
msgstr "Mai"

#: ubuntu-mate-welcome:1090
msgid "Jun"
msgstr "Jun"

#: ubuntu-mate-welcome:1091
msgid "Jul"
msgstr "Jul"

#: ubuntu-mate-welcome:1092
msgid "Aug"
msgstr "Aoû"

#: ubuntu-mate-welcome:1093
msgid "Sep"
msgstr "Sep"

#: ubuntu-mate-welcome:1094
msgid "Oct"
msgstr "Oct"

#: ubuntu-mate-welcome:1095
msgid "Nov"
msgstr "Nov"

#: ubuntu-mate-welcome:1096
msgid "Dec"
msgstr "Déc"

#: ubuntu-mate-welcome:1616
msgid "MB"
msgstr "MB"

#: ubuntu-mate-welcome:1617
msgid "MiB"
msgstr "MiB"

#: ubuntu-mate-welcome:1618
msgid "GB"
msgstr "GB"

#: ubuntu-mate-welcome:1619
msgid "GiB"
msgstr "GiB"

#: ubuntu-mate-welcome:1627
msgid "Could not gather data."
msgstr "Impossible de rassembler les données."

#: ubuntu-mate-welcome:1926
msgid "Raspberry Pi Partition Resize"
msgstr "Modifier la Taille de la Partition du Raspberry Pi"

#: ubuntu-mate-welcome:1943
msgid "Root partition has been resized."
msgstr "La taille de la partition Root a été modifiée."

#: ubuntu-mate-welcome:1943
msgid "The filesystem will be enlarged upon the next reboot."
msgstr "Le système de fichiers sera élargi au prochain démarrage."

#: ubuntu-mate-welcome:1949 ubuntu-mate-welcome:1951 ubuntu-mate-welcome:1953
#: ubuntu-mate-welcome:1955
msgid "Don't know how to expand."
msgstr "Impossible d'étendre la partition."

#: ubuntu-mate-welcome:1949
msgid "does not exist or is not a symlink."
msgstr "n'existe pas ou n'est pas un lien symbolique."

#: ubuntu-mate-welcome:1951
msgid "is not an SD card."
msgstr "n'est pas une carte SD."

#: ubuntu-mate-welcome:1953
msgid "Your partition layout is not currently supported by this tool."
msgstr "La disposition de vos partitions n'est pas supportée par cet outil."

#: ubuntu-mate-welcome:1955
msgid "is not the last partition."
msgstr "n'est pas la dernière partition."

#: ubuntu-mate-welcome:1957
msgid "Failed to run resize script."
msgstr "Echec lors de l'éxecution du script de redimensionnement."

#: ubuntu-mate-welcome:1957
msgid "The returned error code is:"
msgstr "Le code d'erreur est : "

#: ubuntu-mate-welcome:2160
msgid "Open Source"
msgstr "Open Source"

#: ubuntu-mate-welcome:2162
msgid "Proprietary"
msgstr "Propriétaire"

#: ubuntu-mate-welcome:2278
msgid ""
"Sorry, Welcome could not feature any software for this category that is "
"compatible on this system."
msgstr ""
"Désolé, Bienvenue n'a pas pu afficher de logiciels compatibles dans cette "
"catégorie."

#: ubuntu-mate-welcome:2490
msgid ""
"An error occurred while launching PROGRAM_NAME. Please consider re-"
"installing the application."
msgstr ""
"Une erreur est survenue au moment de lancer PROGRAM_NAME. Veuillez "
"considérer réinstaller cette application."

#: ubuntu-mate-welcome:2491
msgid "Command:"
msgstr "Commande :"

#: ubuntu-mate-welcome:2573
msgid "Uses a new or updated source."
msgstr "Utilise une source nouvelle ou mise à jour."

#: ubuntu-mate-welcome:2575
msgid "Now works for various versions of Ubuntu."
msgstr "Fonctionne désormais avec plusieurs versions d'Ubuntu."

#: ubuntu-mate-welcome:2577
msgid "Now a snappy package."
msgstr "Est désormais un paquet rapide."

#: ubuntu-mate-welcome:2579
msgid "General maintenance."
msgstr "Maintenance générale."

#: ubuntu-mate-welcome:2581
msgid "Broken or problematic source."
msgstr "Source erronée ou problématique."

#: ubuntu-mate-welcome:2583
msgid "Not yet available for some releases."
msgstr "Pas encore disponible pour certaines versions."

#: ubuntu-mate-welcome:2585
msgid "No longer works for some releases."
msgstr "Ne fonctionne plus pour une raison inconnue."

#: ubuntu-mate-welcome:2587
msgid "Not suitable for production machines."
msgstr "Ne fonctionne pas pour des machines normales."

#: ubuntu-mate-welcome:2589
msgid "Requires further testing."
msgstr "A besoin de plus de tests."

#: ubuntu-mate-welcome:2591
msgid "Does not meet our standards to be featured."
msgstr "Ne correspond pas à nos standards pour faire parti de la sélection."

#: ubuntu-mate-welcome:2762
msgid "applications found."
msgstr "applications trouvées."

#: ubuntu-mate-welcome:2766
#, fuzzy
msgid "proprietary applications are hidden."
msgstr "applications propriétaires sont cachées."

#~ msgid "Listing packages..."
#~ msgstr "Listing des paquets..."

#~ msgid "Removing software..."
#~ msgstr "Désinstallation du logiciel..."

#~ msgid "Installing software..."
#~ msgstr "Installation du logiciel..."
